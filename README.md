# Flight Reservation System

The following application is a three tier distributed application for a airline company. The main features are:
- Create, update and delete flights as an administrator
- See flights as an administrator/regular user
 - See flight details page with hours in local time and city names and countries.

## Prerequisites
In order to build and execute the application, you need to have the following resources installed:
- JDK version >= 1.8.0
- Maven (or Eclipse IDE/InteliJ IDE)
- Tomcat server version >= 1.7.0.

## Build
In order to build the application, you must execute a clean install on folders flight-presentation, flight-data-access and flight with the following command:
```
mvn clean install
```
After Maven has installed and set the above modules in the local repository, get to directory flight-businnes and execut the following commands:
```
mvn clean install
mvn tomcat7:run
```
The Tomcat server will start at port 8080, so make sure no other service uses that port on local host.
For the database, take the mysql dump and use the Mysql Workbench to import the database on your computer. If you're using mysql from command line, execute the following instruction with the appropriate username:
```
mysql -u username -p flightsystem < flightsystem.sql
```
## Execute
Go to a web browser and access the following url:
```
http://localhost:8080/login
```
Log in with an administrator account configured from the database. Log in with a user account configured from the database.From the log in page you will reach all other pages by means of redirect buttons and hrefs.