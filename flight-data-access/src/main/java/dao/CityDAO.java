package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import interfaces.CityDAOInterface;
import model.City;
import model.Flight;
import util.HibernateUtil;

/**
 * The DAO class for city models.
 * @author Rares Moldovan
 *
 */
public class CityDAO implements CityDAOInterface{

	/**
	 * Retrieves the city object based on the id.
	 * @param id 
	 */
	public City findById(int id)
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		Query query = currentSession.createQuery("from City where id=:value");
		query.setInteger("value", id);
		City city = (City)query.uniqueResult();
		transaction.commit();
		return city;
	}
	
	/**
	 * Retrieves the city object based on a custom column.
	 * @param column The column to be verified.
	 * @param value The value to be compared with.
	 */
	public City findByCustom(String column, String value) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		Query query = currentSession.createQuery("from City where " + column + "=:value");
		query.setString("value", value);
		City city = (City)query.uniqueResult();
		transaction.commit();
		return city;
	}
	
	/**
	 * Inserts the city model into the database.
	 * @param city The city object to be inserted.
	 */
	public void insert(City city) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		currentSession.save(city);
		transaction.commit();
	}
	
	/**
	 * Updates the city model in the database.
	 * @param city The city object to be inserted.
	 */
	public void update(City city)
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		currentSession.update(city);
		transaction.commit();
	}
	
	/**
	 * Deletes the city in the database with the specified id.
	 * @param id
	 */
	public void delete(int id) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();		
		currentSession.createQuery("delete from City where id=:idParameter").setLong("idParameter", id).executeUpdate();
		transaction.commit();
	}
	
	/**
	 * Retrieves all cities in the database in a list.
	 */
	public List<City> getAll()
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();	
		Query query = currentSession.createQuery("from City");
		@SuppressWarnings("unchecked")
		List<City> cities = query.list();
		ArrayList<City> finalList = new ArrayList<City>();
		for(City city : cities) {
				
			finalList.add(city);
		}
		transaction.commit();
		return finalList;
	}
}
