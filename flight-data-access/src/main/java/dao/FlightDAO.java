package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import interfaces.CityDAOInterface;
import interfaces.FlightDAOInterface;
import model.Flight;
import util.HibernateUtil;

/**
 * Implementation of DAO pattern for the flight model
 * @author Rares Moldovan.
 *
 */
public class FlightDAO implements FlightDAOInterface {

	/**
	 * Retrieves the flight object based on the id.
	 * @param id 
	 */
	public Flight findById(int id)
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		Query query = currentSession.createQuery("from Flight where id=:value");
		query.setInteger("value", id);
		Flight flight = (Flight)query.uniqueResult();
		transaction.commit();
		return flight;
	}
	
	/**
	 * Retrieves the flight object based on a custom column.
	 * @param column The column to be verified.
	 * @param value The value to be compared with.
	 */
	public Flight findByCustom(String column, String value) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		Query query = currentSession.createQuery("from City where " + column + "=:value");
		query.setString("value", value);
		Flight flight = (Flight)query.uniqueResult();
		transaction.commit();
		return flight;
	}
	
	/**
	 * Inserts the flight model into the database.
	 * @param flight The flight object to be inserted.
	 */
	public int insert(Flight flight) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		currentSession.save(flight);
		transaction.commit();
		return flight.getId();
	}
	
	/**
	 * Updates the flight model in the database.
	 * @param flight The flight object to be inserted.
	 */
	public void update(Flight flight)
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		currentSession.update(flight);
		transaction.commit();
	}
	
	/**
	 * Deletes the flight in the database with the specified id.
	 * @param id
	 */
	public void delete(int id) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();		
		currentSession.createQuery("delete from Flight where id=:idParameter").setLong("idParameter", id).executeUpdate();
		transaction.commit();
	}
	
	/**
	 * Retrieves all flights in the database in a list.
	 */
	public List<Flight> getAll()
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();	
		Query query = currentSession.createQuery("from Flight");
		@SuppressWarnings("unchecked")
		List<Flight> flights = query.list();
		ArrayList<Flight> finalList = new ArrayList<Flight>();
		for(Flight F : flights) {
				
			finalList.add(F);
		}
		transaction.commit();
		return finalList;
	}
}
