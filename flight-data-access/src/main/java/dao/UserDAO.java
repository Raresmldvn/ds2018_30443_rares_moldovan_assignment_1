package dao;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import interfaces.UserDAOInterface;
import model.User;
import util.HibernateUtil;

/**
 * The DAO class for user models.
 * @author Rares Moldovan
 *
 */
public class UserDAO implements UserDAOInterface {
	
	/**
	 * Retrieves the user object based on the id.
	 * @param id 
	 */
	public User findById(int id)
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		Query query = currentSession.createQuery("from User where id=:value");
		query.setInteger("value", id);
		User user =(User)query.uniqueResult();
		transaction.commit();
		return user;
	}
	
	/**
	 * Retrieves the user object based on a custom column.
	 * @param column The column to be verified.
	 * @param value The value to be compared with.
	 */
	public User findByCustom(String column, String value) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		System.out.println(column + " " + value);
		Query query = currentSession.createQuery("from User where " + column + "=:value");
		query.setString("value", value);
		System.out.println(value);
		User user = (User)query.uniqueResult();
		System.out.println(user);
		transaction.commit();
		return user;
	}
	
	/**
	 * Inserts the user model into the database.
	 * @param user The user object to be inserted.
	 */
	public void insert(User user) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		currentSession.save(user);
		transaction.commit();
	}
	
	/**
	 * Updates the user model in the database.
	 * @param city The user object to be inserted.
	 */
	public void update(User user)
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();
		currentSession.update(user);
		transaction.commit();
	}
	
	/**
	 * Deletes the user in the database with the specified id.
	 * @param id
	 */
	public void delete(int id) 
	{
		Session currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = currentSession.beginTransaction();		
		currentSession.createQuery("delete from User where id=:idParameter").setLong("idParameter", id).executeUpdate();
		transaction.commit();
	}
}
