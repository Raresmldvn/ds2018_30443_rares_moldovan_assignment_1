package interfaces;

import java.util.List;

import model.Flight;

/**
 * DAO interface of the flight model.
 * @author Rares Moldovan
 *
 */
public interface FlightDAOInterface {

	Flight findById(int id);

	Flight findByCustom(String column, String value);

	int insert(Flight flight);

	void update(Flight flight);

	void delete(int id);
	
	public List<Flight> getAll();

}