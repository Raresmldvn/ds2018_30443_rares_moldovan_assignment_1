package interfaces;

import model.User;

/**
 * DAO interface of the user model.
 * @author Rares Moldovan
 *
 */
public interface UserDAOInterface {

	User findById(int id);

	User findByCustom(String column, String value);

	void insert(User user);

	void update(User user);

	void delete(int id);

}