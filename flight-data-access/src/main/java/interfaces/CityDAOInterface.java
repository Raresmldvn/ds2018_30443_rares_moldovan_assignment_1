package interfaces;

import java.util.List;

import model.City;

/**
 * DAO interface of the city model.
 * @author Rares Moldovan
 *
 */
public interface CityDAOInterface {

	City findById(int id);

	City findByCustom(String column, String value);

	void insert(City flight);

	void update(City flight);

	void delete(int id);
	
	public List<City> getAll();

}