package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Flight entity class.
 * @author Rares Moldovan
 *
 */
@Entity
@Table(name = "flight")
public class Flight {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "flight_id")
	private int id;
	
	@Column(name="flight_number")
	private int number;
	
	@Column(name="airplane_type")
	private String airplaneType;
	
	@ManyToOne
	@JoinColumn(name = "departure_city_id", nullable = false)
	private City departureCity;
	
	@ManyToOne
	@JoinColumn(name = "arrival_city_id", nullable = false)
	private City arrivalCity;
	
	@Column(name = "departure_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date departureDate;
	
	@Column(name = "arrival_date", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date arrivalDate;

	public Flight() {}
	
	public Flight(int id, int number, String airplaneType, City departureCity, City arrivalCity, Date departureDate,
			Date arrivalDate) {
		this.id = id;
		this.number = number;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	
	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public String toString()
	{
		String representation = "";
		representation += "Id: " + this.id + "\n";
		representation +="Number: " + this.number + "\n";
		representation +="Type: " + this.airplaneType + "\n";
		representation +="Departure: " + this.departureCity.toString() + " " + this.departureDate.toString() + "\n";
		representation +="Arrival: " + this.arrivalCity.toString() + " " + this.arrivalDate.toString() + "\n";
		return representation;
	}
	
}
