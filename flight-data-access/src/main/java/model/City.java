package model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * City entity class.
 * @author Rares Moldovan
 *
 */
@Entity
@Table(name = "city")
public class City {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "city_id")
	private int id;
	
	@Column(name="latitude")
	private double latitude;
	
	@Column(name="longitude")
	private double longitude;

	@OneToMany(mappedBy = "arrivalCity", fetch = FetchType.EAGER)
	private Set<Flight> arrivalFlights = new HashSet();
	
	@OneToMany(mappedBy = "departureCity", fetch = FetchType.EAGER)
	private Set<Flight> departureFlights = new HashSet();
	

	public City() {}
	
	public City(int id, double latitude, double longitude) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Set<Flight> getArrivalFlights() {
		return arrivalFlights;
	}

	public void setArrivalFlights(Set<Flight> arrivalFlights) {
		this.arrivalFlights = arrivalFlights;
	}

	public Set<Flight> getDepartureFlights() {
		return departureFlights;
	}

	public void setDepartureFlights(Set<Flight> departureFlights) {
		this.departureFlights = departureFlights;
	}
	
	public String toString()
	{
		return "Id: " + id + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude;
	}
	
	
}
