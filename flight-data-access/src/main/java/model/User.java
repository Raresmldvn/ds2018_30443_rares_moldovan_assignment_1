package model;

import javax.persistence.*;

/**
 * User entity class.
 * @author Rares Moldovan
 *
 */
@Entity
@Table(name = "user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private int id;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "is_admin")
	private boolean isAdmin;
	
	public User() {}
	
	public User(int id, String email, String password)
	{
		this.id = id;
		this.email = email;
		this.password = password;
	}
	
	public int getId() 
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String toString()
	{
		String representation = Integer.toString(id);
		representation += " " + email + " " + password + " " + isAdmin;
		return representation;
	}
}
