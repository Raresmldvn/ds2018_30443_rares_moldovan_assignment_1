package views;

import util.StringUtil;

/**
 * The LogIn view class.
 * @author rares
 *
 */
public class LogInView extends AbstractView {
	
	public static final String PLACEHOLDER = "<java/>";
	public static final String ERROR_MESSAGE = "You provided an incorrect username or password.";
	
	public LogInView() {
		
		this.sourceFileName ="src/main/resources/LogIn.html";
	}
	
	/**
	 * Adds and error message into the log in HTML file.
	 */
	public void addErrorMessage()
	{
		this.buildHtmlContent();
		this.htmlContent = StringUtil.injectText(this.htmlContent, PLACEHOLDER, ERROR_MESSAGE);
	}
}
