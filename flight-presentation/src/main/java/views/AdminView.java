package views;

import java.util.List;

import dto.CityDTO;
import dto.FlightDTO;
import util.StringUtil;

/**
 * View class for the administrator page.
 * @author Rares Moldovan
 *
 */
public class AdminView extends AbstractView{
	
	public AdminView()
	{
		this.sourceFileName = "src/main/resources/Admin.html";
	}

	/**
	 * Insert the flight content based on the list of flights and cities.
	 * @param flight The flight object.
	 * @param cities The list of cities.
	 */
	public void insertFlightContent(FlightDTO flight, List<CityDTO> cities)
	{
		this.buildHtmlContent();
		htmlContent = StringUtil.injectText(this.htmlContent, "javaId", "value=\"" + flight.getId() + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaNumber", "value=\"" + flight.getNumber() + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaType", "value=\"" + flight.getType() + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaDepDate", "value=\"" + StringUtil.dateToDatetime(flight.getDepartureTime()) + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaArrDate", "value=\"" + StringUtil.dateToDatetime(flight.getArrivalTime()) + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaDepCoord", "value=\"" + StringUtil.getCityOptionList(cities, flight.getDepartureCityLat(), flight.getDepartureCityLong()) + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaArrCoord", "value=\"" + StringUtil.getCityOptionList(cities, flight.getArrivalCityLat(), flight.getArrivalCityLong()) + "\"");
	}
	
	/**
	 * Insert the city content into the HTML in a select tag.
	 * @param cities The list of cities.
	 */
	public void insertCityContent(List<CityDTO> cities)
	{
		this.buildHtmlContent();
		htmlContent = StringUtil.injectText(this.htmlContent, "javaDepCoord", "value=" + StringUtil.getCityOptionList(cities, 0, 0) + "\"");
		htmlContent = StringUtil.injectText(this.htmlContent, "javaArrCoord", "value=" + StringUtil.getCityOptionList(cities, 0, 0) + "\"");
	}
}
