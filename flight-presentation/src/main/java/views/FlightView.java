package views;

import dto.FlightDTO;
import util.StringUtil;

/**
 * The flight view class.
 * @author Rares Moldovan
 *
 */
public class FlightView extends AbstractView{

	public static final String PLACEHOLDER = "<java/>";
	
	public FlightView() {
		
		this.sourceFileName ="src/main/resources/Flight.html";
	}
	
	/**
	 * Adds flight data in the form of a table.
	 * @param flight The flight object.
	 * @param departureCity The computed departure city (out of coordinates).
	 * @param arrivalCity The computed arrival city (out of coordinates).
	 */
	public void addFlightData(FlightDTO flight, String departureCity, String arrivalCity)
	{
		this.buildHtmlContent();
		this.htmlContent = StringUtil.injectText(this.htmlContent, "javaNumber", Integer.toString(flight.getNumber()));
		this.htmlContent = StringUtil.injectText(this.htmlContent, "javaType", flight.getType());
		this.htmlContent = StringUtil.injectText(this.htmlContent, "javaDepartureHour", flight.getDepartureTime());
		this.htmlContent = StringUtil.injectText(this.htmlContent, "javaDepartureCity", departureCity);
		this.htmlContent = StringUtil.injectText(this.htmlContent, "javaArrivalHour", flight.getArrivalTime());
		this.htmlContent = StringUtil.injectText(this.htmlContent, "javaArrivalCity", arrivalCity);
	}
}