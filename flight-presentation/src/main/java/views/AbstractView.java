package views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Base class for all view classes in the application. Contains logic for loading a HTML file into a string.
 * @author Rares Moldovan
 *
 */
public abstract class AbstractView {
	
	protected String sourceFileName;
	
	protected String htmlContent;
	
	/**
	 * Build the html string in case it doesn't exist and retrieve it.
	 * @return The HTML string attribute.
	 */
	public String getHtmlContent() {
		
		if(this.htmlContent==null) {
			this.buildHtmlContent();
		}
		return this.htmlContent;
	}
	
	/**
	 * Load the HTML file with the name set in soruceFileName attribute into the htmlContent attribute.
	 */
	protected void buildHtmlContent() {
		File sourceFile = new File(this.sourceFileName);
		try {
            byte[] bytes = Files.readAllBytes(sourceFile.toPath());
            this.htmlContent = new String(bytes, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
