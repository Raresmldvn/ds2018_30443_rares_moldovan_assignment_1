package views;

import java.util.List;

import dto.FlightDTO;
import util.StringUtil;

/**
 * Flight view class.
 * @author Rares Moldovan
 *
 */
public class FlightsView extends AbstractView{

	public static final String PLACEHOLDER = "<java/>";
	
	public FlightsView() {
		
		this.sourceFileName ="src/main/resources/Flights.html";
	}
	
	/**
	 * Inserts the table data computed out of the list of flights.
	 * @param flights The list of flight objects.
	 */
	public void addTableData(List<FlightDTO> flights)
	{
		this.buildHtmlContent();
		this.htmlContent = StringUtil.injectTableRows(htmlContent, PLACEHOLDER, flights);
	}
}
