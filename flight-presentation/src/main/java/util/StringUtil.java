package util;

import java.util.List;

import dto.CityDTO;
import dto.FlightDTO;

/**
 * Utility class for string manipulation using data transfer objects.
 * @author Rares Moldovan
 *
 */
public class StringUtil {
	
	/**
	 * Injects a new string in the source string, in place of the placeholder.
	 * @param source The string in which we inject.
	 * @param placeholder The place where we need to inject.
	 * @param value The injected value.
	 * @return The updated string.
	 */
	public static String injectText(String source, String placeholder, String value)
	{
		return source.replace(placeholder, value);
	}
	
	/**
	 * Injects table rows into a source string based on a list of flights and a place holder.
	 * @param source The string in which we inject.
	 * @param placeHolder The place where we need to inject.
	 * @param flights The list of flights.
	 * @return The updated source string.
	 */
	public static String injectTableRows(String source, String placeHolder, List<FlightDTO> flights)
	{
		String tableData = "";
		for(FlightDTO flight : flights) {
			String tableRow = "<tr>" + "\n";
			tableRow += "<td> <a href=\"/flight?id=" + flight.getId() +"\">" + flight.getNumber() + " </a></td>" + "\n";
			tableRow += "<td>" + flight.getType() + " </td>" + "\n";
			tableRow += "<td> " + flight.getDepartureTime() + " </td>" + "\n";
			tableRow += "<td> " + flight.getDepartureCityLat() + " " + flight.getDepartureCityLong() + " </td>" + "\n";
			tableRow += "<td> " + flight.getArrivalTime() + "</td>" + "\n";
			tableRow += "<td> " + flight.getArrivalCityLat() + " " + flight.getDepartureCityLong() + "</td>" + "\n";
			tableRow += "<tr>" + "\n";
			tableData += tableRow;
		}
		return injectText(source, placeHolder, tableData);
	}
	
	/**
	 * Gets an option HTML tag based on the latitude and logitude, plus a boolean value which specifies if the option is selected.
	 * @param id The id to be set on value.
	 * @param latitude The latitude given as a number.
	 * @param longitude The longitude given as a number.
	 * @param selected The flag which specifies if the option is selected.
	 * @return The HTML tag.
	 */
	public static String getCityOptions(int id, double latitude, double longitude, boolean selected)
	{
		return "<option value=\"" + id + "\" " + (selected ?  "selected=\"true\"" : "") +   ">" + latitude + " " + longitude + "</option>";
	}
	
	/**
	 * Retrieves a list of options for a select HTML input based on a list of cities and a selected option,
	 * @param cities The list of city data transfer objects.
	 * @param selectedCityLatitude The selected latitude.
	 * @param selectedCityLongitude The selected longitude.
	 * @return The HTML tags for the select input type.
	 */
	public static String getCityOptionList(List<CityDTO> cities, double selectedCityLatitude, double selectedCityLongitude) {
		
		String result = "";
		for(CityDTO city : cities) {
			result += getCityOptions(city.getId(), city.getLatitude(), city.getLongitude(), city.getLatitude() == selectedCityLatitude && city.getLongitude() == selectedCityLongitude) + "\n";
		}
		return result;
	}
	
	/**
	 * Converts a date in HTML format into date time representation with the "T" separator.
	 * @param dateFormat The initial representation.
	 * @return The datetime representation.
	 */
	public static String dateToDatetime(String dateFormat)
	{
		String day = dateFormat.substring(0, 2);
		String month = dateFormat.substring(3, 5);
		String year = dateFormat.substring(6, 10);
		String hour = dateFormat.substring(11,13);
		String minute = dateFormat.substring(14, 16);
		return year + "-" + day + "-" + month + "T" + hour + ":" + minute;
	}
	
}
