package dto;

/**
 * The flight data transfer object.
 * @author Rares Moldovan
 *
 */
public class FlightDTO {

	private int id;
	private int number;
	private String type;
	private String departureTime;
	private String arrivalTime;
	private double departureCityLat;
	private double departureCityLong;
	private double arrivalCityLat;
	private double arrivalCityLong;
	
	public FlightDTO() {}

	public FlightDTO(int id, int number, String type, String departureTime, String arrivalTime) {
		super();
		this.id = id;
		this.number = number;
		this.type = type;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public double getDepartureCityLat() {
		return departureCityLat;
	}

	public void setDepartureCityLat(double departureCityLat) {
		this.departureCityLat = departureCityLat;
	}

	public double getDepartureCityLong() {
		return departureCityLong;
	}

	public void setDepartureCityLong(double departureCityLong) {
		this.departureCityLong = departureCityLong;
	}

	public double getArrivalCityLat() {
		return arrivalCityLat;
	}

	public void setArrivalCityLat(double arrivalCityLat) {
		this.arrivalCityLat = arrivalCityLat;
	}

	public double getArrivalCityLong() {
		return arrivalCityLong;
	}

	public void setArrivalCityLong(double arrivalCityLong) {
		this.arrivalCityLong = arrivalCityLong;
	}
	
	
}
