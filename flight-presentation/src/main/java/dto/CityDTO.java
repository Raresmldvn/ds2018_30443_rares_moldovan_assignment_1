package dto;

/**
 * The city data transfer object.
 * @author Rares Moldovan
 *
 */
public class CityDTO {
	
	private int id;
	private double latitude;
	private double longitude;
	
	public CityDTO() {}
	
	public CityDTO(int id, double latitude, double longitude) {
		super();
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
}
