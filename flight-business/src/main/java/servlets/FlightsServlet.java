package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.FlightDTO;
import services.FlightService;
import views.FlightsView;

/**
 * Servlet for the /flights request.
 * @author Rares Moldovan
 *
 */
public class FlightsServlet extends HttpServlet{
	
	private FlightService flightService;
	
	public FlightsServlet() 
	{
		this.flightService = new FlightService();
	}
	
	private static final long serialVersionUID = -4751096228274971485L;
	
	/**
	 * Returns the flight HTML page as response to a get on the flights URL.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FlightsView flightsView = new FlightsView();
		List<FlightDTO> flights = flightService.getAllFlights();
		flightsView.addTableData(flights);
		response.getWriter().print(flightsView.getHtmlContent());
	}
	
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}