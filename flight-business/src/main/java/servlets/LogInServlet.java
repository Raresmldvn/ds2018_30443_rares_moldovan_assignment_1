package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import views.LogInView;
import services.UserService;

/**
 * Servlet for the /login request.
 * @author Rares Moldovan
 *
 */
public class LogInServlet extends HttpServlet{
	
	private static final long serialVersionUID = -4751096228274971485L;
	
	private UserService userService;
	
	public LogInServlet() 
	{
		this.userService = new UserService();
	}
	

	/**
	 * Returns the log in HTML page as response to a GET request.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		LogInView logInView = new LogInView();
		
		//If error parameter is present, inject error message
		String error = request.getParameter("err");
		if(error!=null) {
			logInView.addErrorMessage();
		}
		response.getWriter().print(logInView.getHtmlContent());
	}
	
	
	/**
	 * Response to the POST request at url /login. Redirects based on log in status.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String username = request.getParameter("email");
		String password = request.getParameter("password");
		int code = userService.logIn(username, password);
		
		//If username and(or) password are not correct, redirect to error page
		if(code==UserService.BAD_REQUEST) {
			response.sendRedirect(request.getRequestURI() +"?err=1");
			return;
		} 
		
		//Set cookies after log in. Cookies keep username and role (so that the role doesn't have to be searched in the DB at every request).
		Cookie userCookie = new Cookie("username", username);
		response.addCookie(userCookie);
		Cookie roleCookie;
		String redirectURI;
		if(code==UserService.IS_USER) {
			roleCookie = new Cookie("role", "user");
			redirectURI = "/flights";
		} else {
			roleCookie = new Cookie("role", "admin");
			redirectURI = "/admin";
		}
		roleCookie.setMaxAge(600);
		response.addCookie(roleCookie);
		response.sendRedirect(redirectURI);
	}
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}