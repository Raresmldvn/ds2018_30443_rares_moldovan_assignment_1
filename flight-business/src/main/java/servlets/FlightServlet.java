package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.FlightDTO;
import services.FlightService;
import util.LocaleUtil;
import views.FlightView;

/**
 * Servlet for the /flight request.
 * @author Rares Moldovan
 *
 */
public class FlightServlet extends HttpServlet{
	
	private FlightService flightService;
	
	public FlightServlet() 
	{
		this.flightService = new FlightService();
	}
	
	private static final long serialVersionUID = -4751096228274971485L;
	

	/**
	 * Returns the flight HTML page as response to a get on the flight URL.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = request.getParameter("id")!=null ? Integer.parseInt(request.getParameter("id")) : 0;
		if(id==0) {
			return;
		}
		FlightDTO flight = flightService.getFlight(id);
		
		//Call external web service for converting coordinates to city name and for local time computation
		LocaleUtil departureUtil = new LocaleUtil(flight.getDepartureCityLat(), flight.getDepartureCityLong(), flight.getDepartureTime());
		LocaleUtil arrivalUtil = new LocaleUtil(flight.getArrivalCityLat(), flight.getArrivalCityLong(), flight.getArrivalTime());
		departureUtil.setLocalData();
		arrivalUtil.setLocalData();
		flight.setDepartureTime(departureUtil.getLocalTime());
		flight.setArrivalTime(arrivalUtil.getLocalTime());
		
		//Load the flight page html and inject data based on id parameter
		FlightView flightView = new FlightView();
		flightView.addFlightData(flight, departureUtil.getCity(), arrivalUtil.getCity());
		response.getWriter().print(flightView.getHtmlContent());
	}
	
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}
