package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.UserService;
import views.LogInView;

/**
 * Servlet for the /logout request.
 * @author Rares Moldovan
 *
 */
public class LogOutServlet extends HttpServlet{
	
	private static final long serialVersionUID = -4751096228274971485L;
	
	/**
	 * Response to the POST request at url /login. Unsets cookies and redirects to login page.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Get all cookies from the request and process
		Cookie roleCookie=null, userCookie=null;
		Cookie[] cookies = request.getCookies();
		if(cookies==null) {
			response.sendRedirect("/login");
			return;
		}
		//Unset role and username cookies and redirect to log in page
		int i;
		for (i = 0; i < cookies.length; i++) {
	          if(cookies[i].getName().equals("role")) {
	        	  roleCookie = cookies[i];
	          } else if(cookies[i].getName().equals("username")) {
	        	  userCookie = cookies[i];
	          }
		 }
		if(roleCookie!=null && userCookie!=null) {
			roleCookie.setMaxAge(0);
			userCookie.setMaxAge(0);
			response.addCookie(roleCookie);
			response.addCookie(userCookie);
		}
		 response.sendRedirect("/login");
	}
	
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}
