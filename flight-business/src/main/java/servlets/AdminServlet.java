package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.FlightDTO;
import services.FlightService;
import views.AdminView;

/**
 * Servlet for the /admin request.
 * @author Rares Moldovan
 *
 */
public class AdminServlet extends HttpServlet{
	
	private static final long serialVersionUID = -4751096228274971485L;
	private static final String GET_ACTION = "get";
	private static final String CREATE_ACTION = "create";
	private static final String DELETE_ACTION = "delete";
	private static final String UPDATE_ACTION = "update";
	private FlightService flightService;
	
	public AdminServlet() 
	{
		this.flightService = new FlightService();
	}
	
	/**
	 * Returns the administrator HTML page on response.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Process cookies and redirect if user is not logged in or an administrator
		String role = null;
		Cookie[] cookies = request.getCookies();
		if(cookies==null) {
			response.sendRedirect("/login");
			return;
		}
		int i;
		 for (i = 0; i < cookies.length; i++) {
	          if(cookies[i].getName().equals("role")) {
	        	  role = cookies[i].getValue();
	        	  break;
	          }
	       }
		if(role==null) {
			response.sendRedirect("/login");
			return;
		}
		if(role.equals("user")) {
			cookies[i].setMaxAge(0);
			response.addCookie(cookies[i]);
			response.sendRedirect("/login");
			return;
		}
		
		//Load admin html page with injected content if an id is provided in url
		int id = request.getParameter("id")!=null ? Integer.parseInt(request.getParameter("id")) : 0;
		AdminView adminView = new AdminView();
		if(id==0) {
			adminView.insertCityContent(flightService.getAllCities());
		} else {
			FlightDTO requiredFlight = flightService.getFlight(id);
			if(requiredFlight!=null) {
				adminView.insertFlightContent(requiredFlight, flightService.getAllCities());
			} else {
				adminView.insertCityContent(flightService.getAllCities());
			}
		}
		response.getWriter().print(adminView.getHtmlContent());
	}
	
	/**
	 * Response to the POST request at url /admin. Performs CRUD on flight models.
	 * @param request The HTTP request object.
	 * @param response The HTTP response object.
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		//Receive city add request, noticed by the presence of the latitude parameter
		if(request.getParameter("latitude")!=null) {
			double latitude=0, longitude=0;
			try {
			latitude = Double.parseDouble(request.getParameter("latitude"));
			longitude = Double.parseDouble(request.getParameter("longitude"));
			}catch(java.lang.NumberFormatException e) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			flightService.createCity(latitude, longitude);
			response.sendRedirect(request.getRequestURI());
			return;
		}
		
		int id = request.getParameter("id")!="" ? Integer.parseInt(request.getParameter("id")) : 0;
		String action = request.getParameter("action");
		if(action.equals(GET_ACTION)) {
			response.sendRedirect(request.getRequestURI() + "?id=" + id);
			return;
		}
		if(action.equals(DELETE_ACTION)) {
			flightService.deleteFlight(id);
			response.sendRedirect(request.getRequestURI());
			return;
		}
		if(action.equals(CREATE_ACTION)||action.equals(UPDATE_ACTION)) {
			try {
			int number = Integer.parseInt(request.getParameter("number"));
			String type = request.getParameter("type");
			String departureDate = request.getParameter("dep-date");
			String arrivalDate = request.getParameter("arr-date");
			int departureCityId = Integer.parseInt(request.getParameter("dep-coord"));
			int arrivalCityId = Integer.parseInt(request.getParameter("arr-coord"));
			if(action.equals(CREATE_ACTION)) {
				id = flightService.createFlight(number, type, departureDate, arrivalDate, departureCityId, arrivalCityId);
			} else {
				flightService.updateFlight(id, number, type, departureDate, arrivalDate, departureCityId, arrivalCityId);
			}
			response.sendRedirect(request.getRequestURI() + "?id=" + id);
			}catch(Exception e) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			}
			return;
		}
	}
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}
