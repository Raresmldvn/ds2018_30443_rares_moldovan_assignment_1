package converters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import dto.CityDTO;
import dto.FlightDTO;
import dto.UserDTO;
import model.City;
import model.Flight;
import model.User;

/**
 * Converts model in the domain representation into data transfer objects.
 * @author Rares Moldovan
 *
 */
public class DTOConverter {
	
	/**
	 * Converts flight model to flight DTO.
	 * @param flight
	 * @return Object in dto representation.
	 */
	public static FlightDTO convert(Flight flight)
	{
		if(flight==null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String departureDate = df.format(flight.getDepartureDate());
		String arrivalDate = df.format(flight.getArrivalDate());
		double departureLat = flight.getDepartureCity().getLatitude();
		double departureLong = flight.getDepartureCity().getLongitude();
		double arrivalLat = flight.getArrivalCity().getLatitude();
		double arrivalLong = flight.getArrivalCity().getLongitude();
		FlightDTO dto = new FlightDTO(flight.getId(), flight.getNumber(), flight.getAirplaneType(), departureDate, arrivalDate);
		dto.setDepartureCityLat(departureLat);
		dto.setDepartureCityLong(departureLong);
		dto.setArrivalCityLat(arrivalLat);
		dto.setArrivalCityLong(arrivalLong);
		return dto;
	}
	
	/**
	 * Converts a list of flight models into a dto list.
	 * @param flights
	 * @return The converted list.
	 */
	public static List<FlightDTO> convertAll(List<Flight> flights)
	{
		List<FlightDTO> dtos = new ArrayList<FlightDTO>();
		for(Flight flight : flights) {
			dtos.add(convert(flight));
		}
		return dtos;
	}
	
	/**
	 * Converts a city model to a dto city object.
	 * @param city
	 * @return The converted object.
	 */
	public static CityDTO convert(City city)
	{
		if(city==null) {
			return null;
		}
		return new CityDTO(city.getId(), city.getLatitude(), city.getLongitude());
	}
	
	/**
	 * Converts a list of city models into a list of dtos.
	 * @param cities
	 * @return The converted list.
	 */
	public static List<CityDTO> convertCities(List<City> cities)
	{
		List<CityDTO> dtos = new ArrayList<CityDTO>();
		for(City city: cities) {
			dtos.add(convert(city));
		}
		return dtos;
	}
	
	/**
	 * Converts a user model into a user dto.
	 * @param user
	 * @return The user in dto format.
	 */
	public static UserDTO convert(User user)
	{
		return new UserDTO(user.getId(), user.getEmail(), user.getPassword(), user.getIsAdmin());
	}
}
