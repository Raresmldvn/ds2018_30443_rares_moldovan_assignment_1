package services;

import interfaces.UserDAOInterface;
import model.User;
import dao.UserDAO;

/**
 * Service class for managing user domain actions.
 * @author Rares Moldovan
 *
 */
public class UserService {
	
	public static final int IS_ADMIN = 0;
	public static final int IS_USER = 1;
	public static final int BAD_REQUEST = 2;
	
	private UserDAOInterface userDAO;
	
	public UserService()
	{
		this.userDAO = new UserDAO();
	}
	
	/**
	 * Performs the login operation. Returns error, admin or user via status codes.
	 * @param username The username input.
	 * @param password The password input.
	 * @return The status code of the operation.
	 */
	public int logIn(String username, String password)
	{	
		User user = this.userDAO.findByCustom("email", username);
		if(user==null) {
			return BAD_REQUEST;
		}
		if(!user.getPassword().equals(this.MD5(password))) {
			return BAD_REQUEST;
		}
		return user.getIsAdmin() ? IS_ADMIN : IS_USER;
	}
	
	/**
	 * Converts a string using the MD5 hashing technique.
	 * @param md5 A simple string.
	 * @return The MD5 representation.
	 */
	private String MD5(String md5) {
		try {
		  java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		  byte[] array = md.digest(md5.getBytes());
		  StringBuffer sb = new StringBuffer();
		  for (int i = 0; i < array.length; ++i) {
		         sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
		       }
		        return sb.toString();
		    } catch (java.security.NoSuchAlgorithmException e) {
		    	
		    }
		    return null;
		}
}
