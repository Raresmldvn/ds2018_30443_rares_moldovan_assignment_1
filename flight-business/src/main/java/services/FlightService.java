package services;

import interfaces.CityDAOInterface;
import interfaces.FlightDAOInterface;
import model.City;
import model.Flight;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import converters.DTOConverter;
import dao.CityDAO;
import dao.FlightDAO;
import dto.CityDTO;
import dto.FlightDTO;

/**
 * Service for flight manipulation.
 * @author Rares Moldovan
 *
 */
public class FlightService {
	
	private FlightDAOInterface flightDAO;
	private CityDAOInterface cityDAO;
	
	public FlightService() {
		this.flightDAO = new FlightDAO();
		this.cityDAO = new CityDAO();
	}
	
	/**
	 * Gets a flight by id.
	 * @param id
	 * @return The flight in dto representation.
	 */
	public FlightDTO getFlight(int id)
	{
		return DTOConverter.convert(flightDAO.findById(id));
	}
	
	/**
	 * Retrieves all flight from the database.
	 * @return List of flight data transfer objects
	 */
	public List<FlightDTO> getAllFlights()
	{
		List<Flight> flights = flightDAO.getAll();
		return DTOConverter.convertAll(flights);
	}
	
	/**
	 * Retrieves all cities from the database.
	 * @return List of city data transfer objects.
	 */
	public List<CityDTO> getAllCities()
	{
		List<City> cities = cityDAO.getAll();
		return DTOConverter.convertCities(cities);
	}
	
	/**
	 * Calls data access for deleting a flight with a given id.
	 * @param id The id of the flight to be deleted.
	 */
	public void deleteFlight(int id)
	{
		this.flightDAO.delete(id);
	}
	
	/**
	 * Creates a flight based on input parameters.
	 * @param number The flight number.
	 * @param type The airplane type.
	 * @param departureDate The departure date.
	 * @param arrivalDate The arrival date.
	 * @param departureCityId The departure city id.
	 * @param arrivalCityId The arrival city id.
	 * @return The id of the created flight.
	 */
	public int createFlight(int number, String type, String departureDate, String arrivalDate, int departureCityId,int arrivalCityId) {
		Flight flight = new Flight();
		flight.setNumber(number);
		flight.setAirplaneType(type);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		try {
			Date departure = format.parse(departureDate.substring(0, 10) + " " + departureDate.substring(11, departureDate.length()));
			Date arrival = format.parse(arrivalDate.substring(0,10) + " " + arrivalDate.substring(11, arrivalDate.length()));
			flight.setDepartureDate(departure);
			flight.setArrivalDate(arrival);
		}catch(Exception e) {
			e.printStackTrace();
		}
		City departureCity = new City();
		departureCity.setId(departureCityId);
		City arrivalCity = new City();
		arrivalCity.setId(arrivalCityId);
		flight.setDepartureCity(departureCity);
		flight.setArrivalCity(arrivalCity);
		int id = flightDAO.insert(flight);
		return id;
	}
	
	/**
	 * Updates a flight based on id with values given in input parameters.
	 * @param id The id of the flight to be updated.
	 * @param number The flight number.
	 * @param type The airplane type.
	 * @param departureDate The departure date.
	 * @param arrivalDate The arrival date.
	 * @param departureCityId The departure city id.
	 * @param arrivalCityId The arrival city id.
	 * @return The id of the created flight.
	 */
	public void updateFlight(int id, int number, String type, String departureDate, String arrivalDate, int departureCityId,int arrivalCityId)
	{
		Flight flight= flightDAO.findById(id);
		flight.setNumber(number);
		flight.setAirplaneType(type);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		try {
			Date departure = format.parse(departureDate.substring(0, 10) + " " + departureDate.substring(11, departureDate.length()));
			Date arrival = format.parse(arrivalDate.substring(0,10) + " " + arrivalDate.substring(11, arrivalDate.length()));
			flight.setDepartureDate(departure);
			flight.setArrivalDate(arrival);
		}catch(Exception e) {
			e.printStackTrace();
		}
		City departureCity = new City();
		departureCity.setId(departureCityId);
		City arrivalCity = new City();
		arrivalCity.setId(arrivalCityId);
		flight.setDepartureCity(departureCity);
		flight.setArrivalCity(arrivalCity);
		flightDAO.update(flight);
	}
	
	/**
	 * Creates a new city entry in the database.
	 * @param latitude The latitude of the city.
	 * @param longitude The longitude of the city.
	 */
	public void createCity(double latitude, double longitude)
	{
		City city = new City();
		city.setLatitude(latitude);
		city.setLongitude(longitude);
		cityDAO.insert(city);
	}
}
