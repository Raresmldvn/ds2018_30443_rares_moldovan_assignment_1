package util;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

/**
 * Class that connects to the geonames webservice for retrieval of locale time and city name.
 * @author Rares Moldovan
 *
 */
public class LocaleUtil {

	private static final String API_URL = "http://api.geonames.org/timezoneJSON";
	
	private double latitude;
	private double longitude;
	private Date date;
	private String city;
	private String localTime;
	
	public LocaleUtil(double latitude, double longitude, String date)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		try {
			this.date = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Makes a request to the timezone web service for finding out the city name and the date in local time
	 * based on latitude and longitude.
	 */
	public void setLocalData()
	{
		try {
			//Create query string and initialize connection
			String queryString = "lat=" + latitude + "&" + "lng=" + longitude + "&username=raresmldvn";
			URL url = new URL(API_URL + "?" + queryString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			
			//Read the JSON response into a string
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			String jsonResponse ="";
			while ((inputLine = in.readLine()) != null) {
				jsonResponse += inputLine + "\n";
			}
			in.close();
		
			//Read the JSON data and store it in the attributes
			JSONObject jsonObject = new JSONObject(jsonResponse);
			try {
				this.city = jsonObject.getString("timezoneId");
			}catch(org.json.JSONException e) {
				this.city = "Unknown";
			}
			
			//Add the gmt hour offset
			long hoursInMillis = 60L * 60L * 1000L;
			long hourOffset = jsonObject.getLong("gmtOffset");
			Date newDate = new Date(this.date.getTime() + (hourOffset * hoursInMillis));
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			this.localTime = df.format(newDate);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getCity() {
		return this.city;
	}
	
	public String getLocalTime() {
		return this.localTime;
	}
	
}
